// *Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX дозволяє завантажувати дані без оновлення сторінки. JavaScript може надсилати мережні запити на сервер і підвантажувати нову інформацію за необхідності, а також змінювати її та повертати.



let url = 'https://ajax.test-danit.com/api/swapi/films';

function creatElement(tagName, className, contentText = '') {
  let element = document.createElement(tagName);
  element.className = className;
  element.innerText = contentText;
  return element;
}

function getFilms(url) {
  fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        } else {
          console.log('ERROR')
          throw Error
        }
      })
      .then(result => {
        const ul = creatElement('ul', 'films');
        result.forEach(({name, episodeId, openingCrawl, characters}) => {
            let nameFilm = creatElement('h2', 'title', `episode № ${episodeId}, name film: ${name}`);
            let desc = creatElement('p', 'desc', openingCrawl);
            const li = creatElement('li');
            li.append(nameFilm, desc);
            ul.insertAdjacentElement("afterbegin", li);
            getCharacters(characters, nameFilm);
        });
        document.body.append(ul);
      })
      .catch(function(error) {
        console.log(error);
      })
}

function getCharacters(arrayCharacters, nameFilm) {
  let charactersList = creatElement('div', 'charactersList');
  let requests = arrayCharacters.map(element2 => fetch(element2));
  Promise.all(requests)
      .then(responses => Promise.all(responses.map(r => r.json())))
      .then((result2) =>
        result2.forEach(element => charactersList.insertAdjacentElement('beforeend', creatElement('a', 'characterName', `${element.name}, `)))
      )
      .then(nameFilm.insertAdjacentElement("afterend", charactersList))
}

getFilms(url);
