// * 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Коли ми зчитуємо якусь властивість об’єкта object, але її не має, JavaScript автоматично бере її з прототипу. Якщо об’єкт успадкований від батьківського об’єкта, то він також буде мати доступ до методів батьківського об’єкта. Кожний раз, при виклику будь-якого методу, JavaScript буде шукати починаючи від прототипу успадкованого об’єкта до прототипу батьківського об’єкту.

// * 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Для викликання батьківського конструктора та передаванню в нього аргументів з конструктора класу-нащадка.



class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name(){
    return this._name;
  }
  set name(valueName){
    this._name = valueName;
  }
  get age(){
    return this._age;
  }
  set age(valueAge){
    this._age = valueAge;
  }
  get salary(){
    return this._salary;
  }
  set salary(valueSalary){
    this._salary = valueSalary;
  }
}

class Programmer extends Employee{
  constructor(name, age, salary, lang) {
    super(...arguments);
    this._lang = lang;
  }
  get salary(){
    return super.salary * 3;
  }
}


let frontend = new Programmer('Pup', 45, 1500, 'english');
let hr = new Programmer('Koko', 25, 350, 'spanish');
let bos = new Programmer('Jack', 45, 10000, 'english');

console.log(frontend.name, frontend.age, frontend.salary, frontend._lang);
console.log(hr.name, hr.age, hr.salary, hr._lang);
console.log(bos.name, bos.age, bos.salary, bos._lang);


  