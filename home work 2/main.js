//* Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Найчастіший сценарій використання - обробити відомі винятки, а в разі невідомих помилок, прокинути їх далі. Один з таких прикладів це отримання масиву данних з іншого документа, сервера тощо. В разі відсутності певних властивостей дана конструкція дасть нам змогу це виявити, проінформувати, а також виконати дії за для усунення даних помилок (виконати перезавантаження або показати подальші дії користувачу за для усуненню даних помилок.)


const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


let properties = ['author', 'name', 'price'];

let ul = document.createElement('ul');

let container = document.getElementById('root');

function creatValidList(arrayBooks, arrayProperties) {
  
  if (Array.isArray(arrayBooks) & Array.isArray(arrayProperties)) {
    
    arrayBooks.forEach(element => {
      try {
        let listNotProperties = [];
        
        for (const property of arrayProperties) {
          let isValid = element.hasOwnProperty(property);
    
          if (!isValid) {
            listNotProperties.push(property);
          }
        }

        if (listNotProperties.length === 0) {
          creatList(element, ul);
        } else {
          throw new SyntaxError(`Дані (об'єкт) немістять таких властивостей: ${listNotProperties}`);
        }

      } catch (e) {
        console.log(e.message);
      }
    });

  } else {
    alert('Аргументи до даної функції необхідно передати массивом!');
  }
}
    
function creatList (book, list) {
  
  container.append(list);
  let li = document.createElement('li');
  let content = [];

  for (const [key, value] of Object.entries(book)) {
    content.push(' ' + `${key}: ${value}`);
  }
  
  li.innerText = content;
  ul.insertAdjacentElement("afterbegin", li);
}

creatValidList(books, properties);
