const cardsList = document.querySelector('.cards');

class Card {
  constructor({ body, title, id, userId }) {
    this._body = body;
    this._title = title;
    this._id = id;
    this._userId = userId;
    this.card;
  }

  get userId() {
    return this._userId;
  }

  get id() {
    return this._id;
  }

  get body() {
    return this._body;
  }

  get title() {
    return this._title;
  }

  // render - назва методу що відображає елементи в НТМЛ
  render(parentElement) {
    this.card = document.createElement('div');
    this.card.className = 'card';
    const description = document.createElement('p');
    description.className = 'card__description';
    description.innerText = this._body;
    const title = document.createElement('h3');
    title.className = 'card__title';
    title.innerText = this._title;

    this.card.append(
      description,
      title
    );

    parentElement.insertAdjacentElement("beforeend", this.card);
  }

  addBtnDelete(){
    const btnDelete = document.createElement('button');
    btnDelete.className = 'btn-delete';
    btnDelete.innerText = 'delete';
    btnDelete.addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this._id}`, {
        method: "DELETE",
      }).then(() => {
        this.card.remove();
      });
    });
    this.card.insertAdjacentElement("afterbegin", btnDelete);
  }

}

function getAuthor(newPost){
  let author = document.createElement('div');
  author.className = 'card__author';
  
  fetch("https://ajax.test-danit.com/api/json/users")
    .then(response => {
      if (response.ok) {
        return response.json()
      } else {
        console.log('ERROR')
        throw Error
      }
    })
    .then((users) => {
      users.forEach(({id, name, email}) => {
        if (newPost.userId === id) {
          author.innerHTML = `
          <a class="author-name">Author: ${name}</a>
          <a class="email" href="mailto:${email}">(Email: ${email})</a>`;
        }
      });
    })
    .catch(function(error) {
      console.log(error);
    });
  
    newPost.card.insertAdjacentElement('beforeend', author);
}

function getPostsList(parentElement) {
  fetch("https://ajax.test-danit.com/api/json/posts")
    .then(response => {
      if (response.ok) {
        return response.json()
      } else {
        console.log('ERROR')
        throw Error
      }
    })
    .then((posts) => {
      posts.map((post) => {
        const newPost = new Card(post);
        newPost.render(parentElement);
        newPost.addBtnDelete();
        getAuthor(newPost);
      });
    })
    .catch(function(error) {
      console.log(error);
    });
}

getPostsList(cardsList);


// function getData(url){
//   let response = fetch(url);
//   if (response.ok) {
//     let data = response.json();
//   } else {
//     alert("Помилка HTTP: " + response.status);
//   }
// }