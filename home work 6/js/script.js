const button = document.createElement('button');
button.className = 'btn-create';
button.innerText = 'Знайти по IP';
button.addEventListener("click", showInfo);
document.body.append(button);

async function showInfo() {

    let ipAdressResponse = await fetch('https://api.ipify.org/?format=json');
    let ipAdress = await ipAdressResponse.json();

    let fullInfoResponse = await fetch(`http://ip-api.com/json/${ipAdress.ip}?fields=1572889`);
    let fullInfo = await fullInfoResponse.json();

    let text = document.createElement('p');
    text.innerText = `Континент ${fullInfo.continent}, країна ${fullInfo.country}, регіон ${fullInfo.regionName}, місто ${fullInfo.city}, район ${fullInfo.district}`
    document.body.append(text);
}
  